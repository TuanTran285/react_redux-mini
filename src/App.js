import './App.css';
import DemoReudx_Mini from './DemoRedux_Mini/DemoReudux_Mini';

function App() {
  return (
    <div className="App">
      <DemoReudx_Mini/>
    </div>
  );
}

export default App;
