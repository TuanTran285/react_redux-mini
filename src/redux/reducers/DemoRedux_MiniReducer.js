const stateDefault = {
    number : 0
}

const DemoRedux_MiniReducer = (state = stateDefault, action) => {
    switch(action.type) {
        case 'TANG_GIAM_SL': {
            if(action.tangGiam === 'sub') {
                state.number -= 1
            }else {
                state.number += 1
            }
            return {...state}
        }
        default : return state
    }
}

export default DemoRedux_MiniReducer