import {combineReducers} from 'redux'
import DemoRedux_MiniReducer from './DemoRedux_MiniReducer'

const rootReducer = combineReducers({
    DemoRedux_MiniReducer
})

export default rootReducer