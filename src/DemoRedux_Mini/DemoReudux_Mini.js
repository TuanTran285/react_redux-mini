import React, { Component } from 'react'
import { connect } from 'react-redux'
class DemoReudx_Mini extends Component {
  render() {
    return (
      <div className='p-5'>
        <button onClick={() => this.props.tangGiamSL('sub')} className='btn btn-danger'>-</button>
        <span className='mx-2'>{this.props.number}</span>
        <button onClick={() => this.props.tangGiamSL('add')} className='btn btn-danger'>+</button>
      </div>
    )
  }
}
// láy dữ liệu trên store về thông qua hàm connect của react redux 
const mapStateToProps = (state) => {
  return {
    number: state.DemoRedux_MiniReducer.number
  }
}
const mapDispatchToPorps = (dispatch) => {
  return {
      tangGiamSL: (tangGiam) => {
        dispatch({
          type: 'TANG_GIAM_SL',
          tangGiam
        })
      }
  }
}

// hight oder function 
// truyền vào đối số thứ nhất để lấy được state về
// truyền đối số thứ 2 để gửi lên reducer
export default connect(mapStateToProps, mapDispatchToPorps)(DemoReudx_Mini)